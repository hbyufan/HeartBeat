package com.andaily.web.controller;

import com.andaily.domain.dto.user.SystemSettingDto;
import com.andaily.service.LogService;
import com.andaily.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

/**
 * @author Shengzhao Li
 */
@Controller
@RequestMapping("/system/")
public class SystemController {


    @Autowired
    private UserService userService;
    @Autowired
    private LogService logService;


    /*
    * 设置
    * */
    @RequestMapping(value = "setting.hb", method = RequestMethod.GET)
    public String loadSetting(Model model) {
        SystemSettingDto formDto = userService.loadSystemSettingDto();
        model.addAttribute("formDto", formDto);
        return "user/system_setting";
    }

    /*
    * 设置 提交
    * */
    @RequestMapping(value = "setting.hb", method = RequestMethod.POST)
    public String submitSetting(@ModelAttribute("formDto") @Valid SystemSettingDto formDto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "user/system_setting";
        }
        userService.updateSystemSetting(formDto);
        model.addAttribute("alert", "updateSettingOK");
        return "redirect:setting.hb";
    }


    /**
     * 手动执行清理旧的日志, 根据配置的时间
     *
     * @param model Model
     * @return View
     */
    @RequestMapping(value = "execute_clean_log.hb", method = RequestMethod.POST)
    public String executeCleanLog(Model model) {
        final long amount = logService.executeAutoCleanMonitorLogs();
        model.addAttribute("cleaned_amount", amount).addAttribute("alert", "cleanLogOK");
        return "redirect:setting.hb";
    }

}